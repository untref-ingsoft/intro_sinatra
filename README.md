# Introducción a Sinatra

Para correr los ejemplos usando docker ejecutar el script `ejecutar_docker.sh` o manualmente el siguiente comando:

```
docker run -it --rm -p4567:4567 -v$PWD:/workspace registry.gitlab.com/fiuba-memo2/imagen-para-katas:latest /bin/bash
```

Una vez que el contenedor está corriendo:

1. Instalar las dependencias de ruby con `bundle install`
2. Ejecutar los ejemplos con `ruby 0x_xyz.rb -o 0.0.0.0`
3. Para navegar a la aplicación usar la URL: `http://localhost:4567/` en el browser
