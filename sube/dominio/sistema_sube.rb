class SistemaSube
  def initialize
    @contador = 0
  end

  def registrar_usuario(_numero_tarjeta, _dni_usuario, _nombre_usuario)
    @contador += 1
  end

  def reset
    @contador = 0
  end
end
