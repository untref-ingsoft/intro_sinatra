# frozen_string_literal: true

require_relative '../web_app'
require 'rspec'
require 'rack/test'

xdescribe 'Aplicación Sinatra' do
  include Rack::Test::Methods

  before(:all) do
    post '/reset'
  end

  def app
    Sinatra::Application
  end

  before do
    post '/reset'
  end

  it '/ responde con un mensaje' do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.body.include?('sube')).to be true
  end

  def registrar_tarjeta(numero_tarjeta, dni_usuario='30111222', nombre_usuario='juan perez')
    datos = { numero_tarjeta: numero_tarjeta, dni_usuario: dni_usuario, nombre_usuario: nombre_usuario}
    post '/tarjetas', datos.to_json
    last_response
  end

  def cargar_credito(numero_tarjeta, monto)
    datos = { monto: monto }
    post "/tarjetas/#{numero_tarjeta}", datos.to_json
    last_response
  end

  def pagar_viaje(numero_tarjeta, importe, fecha = Date.today)
    datos = { importe: importe, fecha: fecha }
    post "/tarjetas/#{numero_tarjeta}/viajes", datos.to_json
    last_response
  end

  describe 'registrar_tarjeta' do
    it 'debería incrementar la cantidad de usuarios' do
      response = registrar_tarjeta(123)
      expect(response).to be_ok
      cuerpo_parseado = JSON.parse(response.body)
      expect(cuerpo_parseado['tarjetas_registradas'].to_i).to eq 1
    end

    it 'no deberia permitir registrar tarjetas ya registradas' do
      datos1 = { numero_tarjeta: '123', dni_usuario: '213423234', nombre_usuario: 'juan perez' }
      datos2 = { numero_tarjeta: '123', dni_usuario: '222222222', nombre_usuario: 'diego gonzales' }

      post '/usuarios', datos1.to_json
      expect(last_response).to be_ok

      post '/usuarios', datos2.to_json
      expect(last_response).to be 400
    end

  end

  describe 'cargar_credito' do
    it 'debería incrementar la cantidad de usuarios' do
      numero_tarjeta = '123'
      registrar_tarjeta(numero_tarjeta)
      reponse = cargar_credito(numero_tarjeta, 10)
      expect(reponse).to be_ok
      cuerpo_parseado = JSON.parse(reponse.body)
      expect(cuerpo_parseado['saldo'].to_i).to eq 10
    end
  end

  describe 'pagar_viaje' do
    it '?????' do
      numero_tarjeta = '123'
      registrar_tarjeta(numero_tarjeta)
      cargar_credito(numero_tarjeta, 100)
      response = pagar_viaje(numero_tarjeta, 10)
      expect(response).to be_ok
      cuerpo_parseado = JSON.parse(response.body)
      expect(cuerpo_parseado['saldo'].to_i).to eq 90
    end
  end

  describe 'consultar_saldo' do
    it 'saldo inicial deberia ser 0' do
      numero_tarjeta = '123'
      registrar_tarjeta(numero_tarjeta)
      get "/tarjetas/#{numero_tarjeta}"
      expect(last_response).to be_ok
      cuerpo_parseado = JSON.parse(last_response.body)
      expect(cuerpo_parseado['saldo'].to_i).to eq 0
    end
  end

  describe 'consultar_viajes' do
    it 'viajes inicialmente deberia ser 0' do
      numero_tarjeta = '123'
      registrar_tarjeta(numero_tarjeta)
      get "/tarjetas/#{numero_tarjeta}/viajes"
      expect(last_response).to be_ok
      cuerpo_parseado = JSON.parse(last_response.body)
      expect(cuerpo_parseado['cantidad_total_viajes'].to_i).to eq 0
      expect(cuerpo_parseado['viajes'].size).to eq 0
    end

    it 'la lista de viajes deberia incluir todos los viajes realizado' do
      numero_tarjeta = '123'
      registrar_tarjeta(numero_tarjeta)
      cargar_credito(numero_tarjeta, 100)
      response = pagar_viaje(numero_tarjeta, 10, Date.parse('2024-01-10'))
      get "/tarjetas/#{numero_tarjeta}/viajes"
      expect(last_response).to be_ok
      cuerpo_parseado = JSON.parse(last_response.body)
      expect(cuerpo_parseado['cantidad_total_viajes'].to_i).to eq 0
      expect(cuerpo_parseado['viajes'].size).to eq 0
    end
  end

  describe 'consultar_transacciones' do

    it 'deberia listar todas las transacciones realizadas' do
      numero_tarjeta = '123'
      registrar_tarjeta(numero_tarjeta)
      cargar_credito(numero_tarjeta, 100)
      pagar_viaje(numero_tarjeta, 10, Date.parse('2024-01-10'))
      get "/tarjetas/#{numero_tarjeta}/transacciones"
      expect(last_response).to be_ok
      cuerpo_parseado = JSON.parse(last_response.body)
      expect(cuerpo_parseado['transacciones'][0]['tipo']).to eq 'carga'
      expect(cuerpo_parseado['transacciones'][0]['fecha']).to eq 'no_disponible'
      expect(cuerpo_parseado['transacciones'][0]['importe']).to eq 100
      expect(cuerpo_parseado['transacciones'][1]['tipo']).to eq 'pago'
      expect(cuerpo_parseado['transacciones'][1]['fecha']).to eq '2024-01-10'
      expect(cuerpo_parseado['transacciones'][1]['importe']).to eq 10
    end

    it 'deberia ser inicialmente vacio' do
      numero_tarjeta = '123'
      registrar_tarjeta(numero_tarjeta)
      get "/tarjetas/#{numero_tarjeta}/transacciones"
      expect(last_response).to be_ok
      cuerpo_parseado = JSON.parse(last_response.body)
      expect(cuerpo_parseado['transacciones'].size).to eq 0
    end

  end

end