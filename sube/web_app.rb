require 'sinatra'
require 'sinatra/json'
require_relative './dominio/sistema_sube'

configure do
  set :sistema_sube, SistemaSube.new
end

before do
  if !request.body.nil? && request.body.size.positive?
    request.body.rewind
    @params = JSON.parse(request.body.read, symbolize_names: true)
  end
end

def sistema
  settings.sistema_sube
end

get '/' do
  json({ mensaje: 'sistema sube' })
end

post '/reset' do
  sistema.reset
end

post '/tarjetas' do
  numero_tarjeta = params[:numero_tarjeta]
  dni_usuario = params[:dni_usuario]
  nombre_usuario = params[:nombre_usuario]
  cantidad_usuarios_registrados = sistema.registrar_usuario(numero_tarjeta, dni_usuario,
                                                            nombre_usuario)
  json({ tarjetas_registradas: cantidad_usuarios_registrados })
end

post '/tarjetas/:numero_tarjeta' do
  monto = params[:monto].to_i
  json({ saldo: monto })
end

post '/tarjetas/:numero_tarjeta/viajes' do
  numero_tarjeta = params[:numero_tarjeta]
  importe = params[:importe].to_i
  json({ saldo: 90 })
end

get '/tarjetas/:numero_tarjeta' do
  numero_tarjeta = params[:numero_tarjeta]
  json({ saldo: 0 })
end

get '/tarjetas/:numero_tarjeta/viajes' do
  numero_tarjeta = params[:numero_tarjeta]
  viajes = []
  # {fecha: DateTime.now.strftime('%Y-%m-%d'), importe: 10}, {fecha: DateTime.now.next.strftime('%d%m%Y'), importe: 20}
  json({ cantidad_total_viajes: 0, viajes: viajes })
end